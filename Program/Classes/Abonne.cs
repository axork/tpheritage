﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Abonne
    {
        private int nbMaxEmprunt;
        private string nom;
        private string prenom;
        private int nbEmprunt;

        private List<Media> medias;

        public Abonne(string n, string p)
        {
            nbMaxEmprunt = 1;
            Nom = n;
            Prenom = p;
            nbEmprunt = 0;
            medias = new List<Media>();
        }
                     
        public int NbMaxEmprunt { get => nbMaxEmprunt; set => nbMaxEmprunt = value; }
        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
        public int NbEmprunt { get => nbEmprunt; set => nbEmprunt = value; }

        public override string ToString()
        {
            return "Abonné(e) : " + Prenom + " " + Nom + "\nNombre d'emprunt autorisé : " + nbMaxEmprunt;
        }

        public void Emprunter(Mediatheque m, Media doc)
        {
            if (nbEmprunt < 1)
            {
                m.setEmprunt(doc);

            }
        }
    }
}
