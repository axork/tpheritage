﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Auteur
    {
        private string nom;
        private string prenom;

        public Auteur(string n, string p)
        {
            nom = n;
            prenom = p;
        }

        public override string ToString()
        {
            return "Auteur : " + prenom + " " + nom;
        }

        public string Nom { get => nom; set => nom = value; }
        public string Prenom { get => prenom; set => prenom = value; }
    }
}
