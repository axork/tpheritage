﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class BandeDessinee : Livre
    {
        private int nbAlbum;

        public BandeDessinee(string t, Auteur a, int nbP, int nbA) : base(t, a, nbP)
        {
            NbAlbum = nbA;
        }

        public override string ToString()
        {
            return base.ToString() + "\nNombre d'albums : " + nbAlbum;
        }

        public int NbAlbum { get => nbAlbum; set => nbAlbum = value; }
    }
}
