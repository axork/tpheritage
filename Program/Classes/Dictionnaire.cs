﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Dictionnaire : Media
    {
        private string langue;
        private int nbTomes;

        public Dictionnaire(string t, string l, int nbT) : base(t)
        {
            langue = l;
            nbTomes = nbT;
        }

        public override string ToString()
        {
            return base.ToString() + "\nLangue : " + langue + "\nNombre de tomes : " + nbTomes;
        }

        public string Langue { get => langue; set => langue = value; }
        public int NbTomes { get => nbTomes; set => nbTomes = value; }
    }
}
