﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class DictionnaireBilingue : Dictionnaire
    {
        private string langue2;

        public DictionnaireBilingue(string t, string l, int nbT, string l2) : base(t, l, nbT)
        {
            langue2 = l2;
        }

        public string Langue2 { get => langue2; set => langue2 = value; }

        public override string ToString()
        {
            return base.ToString() + "\nLangue 2 : " + langue2;
        }
    }
}
