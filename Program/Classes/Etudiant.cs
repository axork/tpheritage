﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Etudiant : Abonne
    {
        public Etudiant(string n, string p) : base(n, p)
        {
            base.NbMaxEmprunt = 2;
        }
    }
}
