﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Livre : Media
    {
        private Auteur auteur;
        private int nbPages;

        public Livre(string t, Auteur a, int nbP) : base(t)
        {
            Auteur = a;
            NbPages = nbP;
        }

        public Auteur Auteur { get => auteur; set => auteur = value; }
        public int NbPages { get => nbPages; set => nbPages = value; }

        public override string ToString()
        {
            return base.ToString() + "\nEcrit par : " + auteur + "\nNombre de pages : " + nbPages;
        }
    }
}
