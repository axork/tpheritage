﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Manga : Livre
    {
        private int nbEpisodes;

        public Manga(string t, Auteur a, int nbP, int nbE) : base(t, a, nbP)
        {
            NbEpisodes = nbE;
        }

        public override string ToString()
        {
            return base.ToString() + "\nNombres d'épisodes : " + nbEpisodes;
        }

        public int NbEpisodes { get => nbEpisodes; set => nbEpisodes = value; }
    }
}
