﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public abstract class Media
    {
        private static int id = -1;
        private string titre;        
        private int numeroEnregistrement;
        private bool emprunt;

        public string Titre { get => titre; set => titre = value; }
        public int NumeroEnregistrement { get => numeroEnregistrement; set => numeroEnregistrement = value; }
        public bool Emprunt { get => emprunt; set => emprunt = value; }

        public Media()
        {

        }

        public Media(string t)
        {
            titre = t;
            id++;
            numeroEnregistrement = id;
            Emprunt = false;
        }

        public override string ToString()
        {
            return numeroEnregistrement + " : " + titre;
        }

        public bool plusPetit(Media doc)
        {
            return numeroEnregistrement < doc.NumeroEnregistrement;
        }
    }
}
