﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Program.Classes
{
    public class Mediatheque
    {
        private List<Media> bibli;

        public Mediatheque()
        {
            bibli = new List<Media>();
        }

        public List<Media> Bibli { get => bibli; set => bibli = value; }

        public void ajouter(Media doc)
        {
            bibli.Add(doc);
        }

        public void Afficher()
        {
            foreach(Media m in bibli)
            {
                Console.WriteLine(m);
            }
        }

        public void setEmprunt(Media doc)
        {
            foreach (Media m in bibli)
            {
                if(m == doc)
                {
                    m.Emprunt = true;
                }
            }
        }
    }
}
