﻿using Program.Classes;
using System;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            Mediatheque mediatheque = new Mediatheque();
            Livre hp = new Livre("Harry Potter", new Auteur("Rowling", "J.K"), 240);
            mediatheque.ajouter(hp);
            Livre got = new Livre("Le Trône de fer", new Auteur("Martin", "George R.R."), 789);
            mediatheque.ajouter(got);
            Dictionnaire francais = new Dictionnaire("Dico francais", "Francais", 26);
            mediatheque.ajouter(francais);
            DictionnaireBilingue francobelge = new DictionnaireBilingue("Dico francobelge", "Francais", 26, "Belge");
            mediatheque.ajouter(francobelge);
            Manga onepiece = new Manga("One Piece", new Auteur("Tok", "Chine"), 200, 216);
            mediatheque.ajouter(onepiece);
            BandeDessinee luckyluke = new BandeDessinee("Lucky Luke", new Auteur("Morris", "Maurice"), 52, 81);
            mediatheque.ajouter(luckyluke);

            mediatheque.Afficher();

            Etudiant e = new Etudiant("Marmin", "Jimmy");

            Console.WriteLine(e);
        }
    }
}
